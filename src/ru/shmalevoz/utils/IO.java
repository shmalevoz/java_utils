/*
 * Copyright 2013 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.utils;

import java.io.*;
import java.util.logging.Logger;

/**
 * Сервисные методы работы с файловой системой
 *
 * @author shmalevoz
 */
public class IO {

	private static final Logger log = Log.getLogger(IO.class.getName());

	private static File getJarFile(Object o) {
		return new File(o.getClass().getProtectionDomain().getCodeSource().getLocation().toString());
	}

	/**
	 * Определяет каталог jar файла, содержащий переданный объект
	 *
	 * @param o исследуемый объект
	 * @return каталог jar файла или null в случае если не удалось определить
	 */
	public static String getJarPath(Object o) {

		String retval = null;
		try {
			File f = getJarFile(o);
			retval = f.getParent() + File.separator;
			String rep = "file:";
			if (System.getProperty("os.name").startsWith("Windows")) {
				rep += File.separator;
			}
			retval = retval.replace(rep, "");

		} catch (Exception e) {
		}

		return retval;
	}

	/**
	 * Возвращает имя jar файла, содержащего переданный объект
	 *
	 * @param o - Исследуемый объект
	 * @return Имя jar файла
	 */
	public static String getJarName(Object o) {
		return getJarFile(o).getName();
	}

	/**
	 * Возвращает поток вывода с установленной кодовой страницей
	 *
	 * @param o Исходный поток вывода
	 * @param codepage Кодовая страница
	 * @return Поток вывода с установленной кодовой страницей
	 * @throws UnsupportedEncodingException
	 */
	private static PrintStream getPrintStream(OutputStream o, String codepage) throws UnsupportedEncodingException {
		return new PrintStream(o, true, codepage);
	}
	
	public static final String getConsoleCodepage() {
		if (System.getProperty("os.name").startsWith("Windows")) {
			return "Cp866";
		} else {
			return "UTF-8";
		}
	}

	/**
	 * Устанавливает кодовую страницу стандартных потоков stdout и stderr в
	 * зависимости от текущей системы
	 */
	public static final void setStdCodepage() {
		if (System.getProperty("os.name").startsWith("Windows")) {
			try {
				System.setOut(getPrintStream(System.out, "Cp866"));
				System.setErr(getPrintStream(System.err, "Cp866"));
			} catch (UnsupportedEncodingException ex) {
				log.severe(ex.getMessage());
			}
		}
	}

}
