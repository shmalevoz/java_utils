/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.utils.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Иерархическое дерево именованных элементов
 * @author shmalevoz
 * @param <T> Тип значений элементов дерева
 */
public class Tree<T> {
	
	private final ArrayList<Tree<T>> leafs = new ArrayList<Tree<T>>();
	private Tree parent;
	private String key;
	private T value;
	
	/**
	 * Конструктор
	 */
	public Tree() {
		this(null);
	}
	
	/**
	 * Конструтор
	 * @param v Значение элемента
	 */
	public Tree(T v) {
		this(v, "");
	}
	
	/**
	 * Конструктор
	 * @param v Значение элемента
	 * @param k Идентификатор элемента
	 */
	public Tree(T v, String k) {
		this(v, k, null);
	}
	
	/**
	 * Конструктор
	 * @param v Значение элемента
	 * @param k Идентификатор элемента
	 * @param p Родительский элемент
	 */
	public Tree(T v, String k, Tree p) {
		value = v;
		key = k;
		parent = p;
	}
	
	/**
	 * Добавляет лист к дереву
	 * @param l Добавляемый лист
	 */
	public void add(Tree<T> l) {
		add(l, leafs.size());
	}
	
	/**
	 * Добавляет лист к дереву
	 * @param l Добавляемый лист
	 * @param i Позиция листа
	 */
	public void add(Tree<T> l, int i) {
		l.setParent(this);
		leafs.add(i, l);
		
	}
	
	/**
	 * Возвращает первый найденный лист с искомым ключом
	 * @param k Искомый ключ
	 * @return Лист дерева, null если не найдено
	 */
	public Tree<T> get(String k) {
		return get(k, false);
	}
	
	/**
	 * Возвращает первый найденный лист с искомым ключом
	 * @param k Искомый ключ
	 * @param cascade Флаг поиска во всем дереве
	 * @return Лист дерева, null если не найдено
	 */
	public Tree<T> get(String k, boolean cascade) {
		return findWithKey(this, k, cascade);
	}
	
	/**
	 * Возвращает первый найденный лист с искомым ключом. Поиск в глубину дерева
	 * @param node
	 * @param k
	 * @param cascade
	 * @return 
	 */
	private Tree<T> findWithKey(Tree<T> node, String k, boolean cascade) {
		
		Iterator<Tree<T>> i = node.getLeafs();
		Tree<T> t;
		Tree<T> retval = null;
		
		while (retval == null && i.hasNext()) {
			t = i.next();
			if (k == null ? t.getKey() == null : k.equals(t.getKey())) {
				retval = t;
			} else {
				retval = findWithKey(t, k, cascade);
			}
		}
		
		return retval;
	}
	
	/**
	 * Возвращает список подчиненных элементов
	 * @return 
	 */
	public Iterator<Tree<T>> getLeafs() {
		return leafs.iterator();
	}
	
	/**
	 * Возвращает количество подчиненных
	 * @return 
	 */
	public int getLeafsCount() {
		return leafs.size();
	}
	
	/**
	 * Возвращает подчиненного по указанному индексу
	 * @param index
	 * @return 
	 */
	public Tree<T> getLeafAt(int index) {
		return leafs.get(index);
	}
	
	/**
	 * Возвращает индекс подчиненного элемента
	 * @param n Подчиненный элемент
	 * @return 
	 */
	public int getLeafIndex(Tree<T> n) {
		return leafs.indexOf(n);
	}
	
	/**
	 * Возвращает идентификатор элемента
	 * @return 
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * Возвращает уровень элемента в дереве
	 * @return 
	 */
	public int getLevel() {
		return (isRoot()) ? 0 : getParent().getLevel() + 1;
	}
	
	/**
	 * Возвращает родительский элемент
	 * @return 
	 */
	public Tree getParent() {
		return parent;
	}
	
	/**
	 * Возвращает корневой элемент дерева
	 * @return 
	 */
	public Tree getRoot() {
		return (isRoot()) ? this : parent.getRoot();
	}
	
	/**
	 * Возвращает значение элемента
	 * @return 
	 */
	public T getValue() {
		return value;
	}
	
	/**
	 * Проверяет наличие потомков у элемента
	 * @return 
	 */
	public boolean hasLeafs() {
		return !leafs.isEmpty();
	}
	
	/**
	 * Удаляет подчиненный элемент
	 * @param c 
	 */
	public void remove(Tree c) {
		leafs.remove(c);
	}
	
	/**
	 * Проверяет, является ли элемент корневым
	 * @return 
	 */
	public boolean isRoot() {
		return parent == null;
	}
	
	/**
	 * Устанавливает идентификатор элемента
	 * @param k 
	 */
	public void setKey(String k) {
		key = k;
	}
	
	/**
	 * Устанавливвает родителя элемента
	 * @param p 
	 */
	public void setParent(Tree p) {
		parent = p;
	}
	
	/**
	 * Устанавливает значение элемента
	 * @param v 
	 */
	public void setValue(T v) {
		value = v;
	}
	
	/**
	 * Сравнивает ветвь с переданным объектом
	 * @param o
	 * @return 
	 */
	public boolean equals(Tree o) {
		return getKey() == null ? o.getKey() == null : getKey().equals(o.getKey())
			&& getValue() == null ? o.getValue() == null : getValue().equals(o.getValue());
	}

	/**
	 * Возвращает строковое представление элемента дерева
	 * @return 
	 */
	@Override
	public String toString() {
		return printNode(this, "0");
	}
	
	/**
	 * Печать элемента дерева
	 * @param node
	 * @param prefix 
	 */
	private String printNode(Tree node, String prefix) {
		
		String node_id = node.getKey().isEmpty() ? "<>" : node.getKey() + ":\t";
		String node_value = node.getValue() == null ? "<null>" : node.getValue().toString();
		String retval = prefix.concat("  ").concat(node_id).concat(node_value);
		for (int i = 0; i < node.getLeafsCount(); i++) {
			retval += "\n" + printNode(node.getLeafAt(i), prefix + "/" + i);
		}
		return retval;
	}
	
	/**
	 * Возвращает идентичность поля элемента и объекта
	 * @param node Элемент дерева
	 * @param v Эталонное значение
	 * @param compareKeys Флаг сравниваемого поля true - ключи, иначе значения
	 * @return Равенство поля с объектом
	 */
	private boolean compareNodeFieldWith(Tree<T> node, Object v, boolean compareKeys) {
		if (compareKeys) {
			return node.getKey() == null ? v == null : node.getKey().equals((String) v);
		} 
		return node.getValue() == null ? v == null : node.getValue().equals((T) v);
	}
	
	/**
	 * Проверяет наличие элемента с искомым полем
	 * @param node
	 * @param v
	 * @param cascade
	 * @param compareKeys
	 * @return 
	 */
	private boolean containsNodeWith(Tree<T> node, Object v, boolean cascade, boolean compareKeys) {
		boolean retval = false;
		Iterator<Tree<T>> i = node.getLeafs();
		Tree<T> l;
		while (!retval && i.hasNext()) {
			l = i.next();
			retval = compareNodeFieldWith(l, v, compareKeys);
			if (cascade && !retval) {
				retval = containsNodeWith(l, v, cascade, compareKeys);
			}
		}
		return retval;
	}
	
	/**
	 * Проверяет наличие ключа в пределах текущего подчинения
	 * @param k
	 * @return 
	 */
	public boolean containsKey(String k) {
		return containsKey(k, false);
	}
	
	/**
	 * Проверяет наличие ключа в дереве
	 * @param k Искомый ключ
	 * @param cascade Флаг проверки во всех подчиненных листьях
	 * @return 
	 */
	public boolean containsKey(String k, boolean cascade) {
		return containsNodeWith(this, k, cascade, true);
	}
	
	/**
	 * Проверяет наличие значения в листьях, подчиненных текущему
	 * @param v Искомое значение
	 * @return 
	 */
	public boolean containsValue(T v) {
		return containsValue(v, false);
	}
	
	/**
	 * Проверяет наличие значения в подчиненном дереве
	 * @param v Искомое значение
	 * @param cascade Флаг проверки во всех подчиненных листьях
	 * @return 
	 */
	public boolean containsValue(T v, boolean cascade) {
		return containsNodeWith(this, v, cascade, false);
	}
	
	/**
	 * Заполняет список листьями с искомым значением
	 * @param list
	 * @param node
	 * @param v
	 * @param cascade
	 * @param compareKeys 
	 */
	private void fillListWithField(List<Tree<T>> list, Tree<T> node, Object v, boolean cascade, boolean compareKeys) {
		Iterator<Tree<T>> i = node.getLeafs();
		Tree<T> l;
		while (i.hasNext()) {
			l = i.next();
			if (compareNodeFieldWith(l, v, compareKeys)) {
				list.add(l);
			}
			if (cascade) {
				fillListWithField(list, l, v, cascade, compareKeys);
			}
		}
	}
	
	/**
	 * Возвращает список листьев с искомым значением
	 * @param node
	 * @param v
	 * @param cascade
	 * @param compareKeys
	 * @return 
	 */
	private List<Tree<T>> listWithField(Tree<T> node, Object v, boolean cascade, boolean compareKeys) {
		ArrayList<Tree<T>> retval = new ArrayList<Tree<T>>();
		fillListWithField(retval, node, v, cascade, compareKeys);
		return retval;
	}
	
	/**
	 * Возвращает список листьев с искомым ключом
	 * @param k
	 * @return 
	 */
	public List<Tree<T>> listWithKey(String k)  {
		return listWithKey(k, false);
	}
	
	/**
	 * Возвращает список листьев с искомым ключом
	 * @param k Искомый ключ
	 * @param cascade Флаг поиска во всем подчиненном дереве
	 * @return 
	 */
	public List<Tree<T>> listWithKey(String k, boolean cascade) {
		return listWithField(this, k, cascade, true);
	}
	
	/**
	 * Возвращает список подчиненных листьев с искомым значением
	 * @param v Искомое значение
	 * @return 
	 */
	public List<Tree<T>> listWithValue(T v) {
		return listWithValue(v, false);
	}
	
	/**
	 * Возвращает список подчиненных листьев с искомым значением
	 * @param v Искомое значение
	 * @param cascade Флаг поиска во всех подчиненных листьях
	 * @return 
	 */
	public List<Tree<T>> listWithValue(T v, boolean cascade) {
		return listWithField(this, v, cascade, false);
	}
}
