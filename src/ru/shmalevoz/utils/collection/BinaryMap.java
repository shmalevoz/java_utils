/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Copy from http://pastebin.com/f50a4a9d7
 * Thanks winger
 */
package ru.shmalevoz.utils.collection;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author shmalevoz
 * @param <T1>
 * @param <T2>
 */
public class BinaryMap<T1 extends Comparable<T1>, T2> {
	
	static class Node<T1, T2> {
		T1 key;
		T2 value;
		Node<T1, T2> left, right;
		
		Node(T1 key, T2 value) {
			this.key = key;
			this.value = value;
		}
	}
	private Node<T1, T2> root = null;
	
	/**
	 * Обход дерева элементов и заполнение списка его ключами
	 * @param n
	 * @param a 
	 */
	private void fillKeys(Node n, ArrayList a) {
		if (n != null) {
			a.add(n.key);
			fillKeys(n.left, a);
			fillKeys(n.right, a);
		}
	}
	
	/**
	 * Возвращает перечисление ключей
	 * @return 
	 */
	public Iterator<T1> enumerateKeys() {
		
		ArrayList<T1> keys = new ArrayList<T1>();
		fillKeys(root, keys);
		
		return keys.iterator();
	}
	
	public boolean containsKey(T1 k) {
		Node<T1, T2> x = root;
		while (x != null) {
			int cmp = k.compareTo(x.key);
			if (cmp == 0) {
				return true;
			}
			if (cmp < 0) {
				x = x.left;
			} else {
				x = x.right;
			}
		}
		return false;
	}
	
	public T2 get(T1 k) {
		Node<T1, T2> x = root;
		while (x != null) {
			int cmp = k.compareTo(x.key);
			if (cmp == 0) {
				return x.value;
			}
			if (cmp < 0) {
				x = x.left;
			} else {
				x = x.right;
			}
		}
		return null;
	}
	
	public void add(T1 k, T2 v) {
		Node<T1, T2> x = root, y = null;
		while (x != null) {
			int cmp = k.compareTo(x.key);
			if (cmp == 0) {
				x.value = v;
				return;
			} else {
				y = x;
				if (cmp < 0) {
					x = x.left;
				} else {
					x = x.right;
				}
			}
		}
		Node<T1, T2> newNode = new Node<T1, T2>(k, v);
		if (y == null) {
			root = newNode;
		} else {
			if (k.compareTo(y.key) < 0) {
				y.left = newNode;
			} else {
				y.right = newNode;
			}
		}
	}
	
	public void remove(T1 k) {
		Node<T1, T2> x = root, y = null;
		while (x != null) {
			int cmp = k.compareTo(x.key);
			if (cmp == 0) {
				break;
			} else {
				y = x;
				if (cmp < 0) {
					x = x.left;
				} else {
					x = x.right;
				}
			}
		}
		if (x == null) {
			return;
		}
		if (x.right == null) {
			if (y == null) {
				root = x.left;
			} else {
				if (x == y.left) {
					y.left = x.left;
				} else {
					y.right = x.left;
				}
			}
		} else {
			Node<T1, T2> leftMost = x.right;
			y = null;
			while (leftMost.left != null) {
				y = leftMost;
				leftMost = leftMost.left;
			}
			if (y != null) {
				y.left = leftMost.right;
			} else {
				x.right = leftMost.right;
			}
			x.key = leftMost.key;
			x.value = leftMost.value;
		}
	}
	
	public boolean isEmpty() {
		return root == null;
	}
}