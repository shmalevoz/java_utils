/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.utils.collection;

import java.lang.reflect.Array;

/**
 * Работа с массивами
 * 
 * @author shmalevoz
 */
public class ArrayUtils {
	
	/**
	 * Объединение массивов
	 * @param <T> тип элементов массива
	 * @param a первый массив
	 * @param b второй массив
	 * @return массив из элементов a + b
	 */
	public static <T> T[] concatenate(T[] a, T[] b) {
		
		int aLen = a.length;
		int bLen = b.length;
		
		T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		
		return c;
	}
	
	/**
	 * Объекдинение массивов
	 * @param a
	 * @param b
	 * @return 
	 */
	public static boolean[] concatenate(boolean[] a, boolean[] b) {
		
		int aLen = a.length;
		int bLen = b.length;
		
		boolean[] c = new boolean[aLen + bLen];
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		
		return c;
	}
	
	/**
	 * Объединение массивов
	 * @param a
	 * @param b
	 * @return 
	 */
	public static byte[] concatenate(byte[] a, byte[] b) {
		
		int aLen = a.length;
		int bLen = b.length;
		
		byte[] c = new byte[aLen + bLen];
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		
		return c;
	}
	
	/**
	 * Объединение массивов
	 * @param a
	 * @param b
	 * @return 
	 */
	public static char[] concatenate(char[] a, char[] b) {
		
		int aLen = a.length;
		int bLen = b.length;
		
		char[] c = new char[aLen + bLen];
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		
		return c;
	}
	
	/**
	 * Объединение массивов
	 * @param a
	 * @param b
	 * @return 
	 */
	public static int[] concatenate(int[] a, int[] b) {
		
		int aLen = a.length;
		int bLen = b.length;
		
		int[] c = new int[aLen + bLen];
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		
		return c;
	}
}
