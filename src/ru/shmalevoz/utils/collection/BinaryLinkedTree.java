/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.utils.collection;

import java.util.Iterator;

/**
 *
 * @author shmalevoz
 * @param <T1>
 * @param <T2>
 */
public class BinaryLinkedTree <T1 extends Comparable<T1>, T2> {
	
	/**
	 * Элемент дерева
	 * @param <T2> 
	 */
	public class Node<T2> {
		
		T2 value;
		BinaryLinkedTree<T1, T2> chields;
		
		public Node(T2 v) {
			this.value = v;
			this.chields = new BinaryLinkedTree<T1, T2>();
		}
		
		/**
		 * Возвращает значение элемента
		 * @return 
		 */
		public T2 getValue() {
			return this.value;
		}
		
		/**
		 * Возвращает подчиненное дерево
		 * @return 
		 */
		public BinaryLinkedTree<T1, T2> getChildren() {
			return this.chields;
		}
		
		/**
		 * Возвращает наличие потомков
		 * @return 
		 */
		public boolean hasChildren() {
			return !this.chields.isEmpty();
		}
	}
	
	private BinaryMap<T1, Node<T2>> nodes;
	
	/**
	 * Конструктор
	 */
	public BinaryLinkedTree() {
		nodes = new BinaryMap<T1, Node<T2>>();
	}
	
	/**
	 * Возвращает истину если ветвь пуста
	 * @return 
	 */
	public boolean isEmpty() {
		return nodes.isEmpty();
	}
	
	/**
	 * Проверяет наличие элемента по ключу
	 * @param k
	 * @return 
	 */
	public boolean containsKey(T1 k) {
		return nodes.containsKey(k);
	}
	
	/**
	 * Возвращает перечисление ключей ветви дерева
	 * @return 
	 */
	public Iterator<T1> enumerateKeys() {
		return nodes.enumerateKeys();
	}
	
	/**
	 * Возвращает элемент дерева по имени
	 * @param key
	 * @return 
	 */
	public Node<T2> get(T1 key) {
		return nodes.get(key);
	}
	
	/**
	 * Добавляет значение в дерево
	 * @param key
	 * @param value 
	 */
	public void add(T1 key, T2 value) {
		nodes.add(key, new Node<T2>(value));
	}
	
	/**
	 * Удаляет элемент дерева
	 * @param key 
	 */
	public void remove(T1 key) {
		nodes.remove(key);
	}
	
}
